import 'package:rxdart/rxdart.dart';
import 'package:zanul_pdam/model/user.dart';
import 'package:zanul_pdam/resources/repository.dart';


class UserBloc {
  final repository = Repository();

  final _userFetcher = PublishSubject<List<User>>();
  Observable<List<User>> get getAllUser => _userFetcher.stream;


  fetchAllUser () async{
    List<User> user = await repository.fetchAllUser();
    _userFetcher.sink.add(user);
  }


  dispose(){
    _userFetcher.close();
  }
}

final bloc = UserBloc();

