import 'package:flutter/material.dart';
import 'package:zanul_pdam/bloc/user_bloc.dart';
import 'package:zanul_pdam/model/user.dart';

class ApiDisplay extends StatefulWidget {
  @override
  _ApiDisplayState createState() => _ApiDisplayState();
}

class _ApiDisplayState extends State<ApiDisplay> {

  @override
  void initState() {
    // TODO: implement initState
    bloc.fetchAllUser();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("API DATA"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Container(

        child: StreamBuilder<List<User>>(
          stream: bloc.getAllUser,
          builder: (context,snapshot){
            if (snapshot.hasData){
              return ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int i) => Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                    BoxShadow(color: Color(0x55000000), offset: Offset(1, 1), blurRadius: 7)
                  ],
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(color: Colors.grey)),
                  child: Row(
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Nama : "+snapshot.data[i].name),
                          Text("Email : "+snapshot.data[i].email),
                          Text("Street : "+snapshot.data[i].address.street)
                        ],
                      )
                    ],
                  ),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
