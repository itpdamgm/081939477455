import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:toast/toast.dart';

class ImageDisplay extends StatefulWidget {
  @override
  _ImageDisplayState createState() => _ImageDisplayState();
}

class _ImageDisplayState extends State<ImageDisplay> {
  File imageFile = null;

  _openKamera(BuildContext context) async{

    var pict = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      imageFile = pict;
    });
    
  }
  
  Widget getImage (BuildContext context) {
    if (imageFile==null) {
      return Text("Belum Ada Gambar");
    } else {
      return Image.file(imageFile, height: 400,width: 400,);
    }
  }

    Future<File> _saveImage (File file) async {

      Directory appDocDir = await getApplicationDocumentsDirectory();
      String appDocPath = appDocDir.path;
      final File newImage = await file.copy('$appDocPath/image1.jpg');
      Toast.show("Berhasil Simpan", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      print(appDocPath);
      return newImage;

    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kamera"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              getImage(context),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    onPressed: (){
                      _openKamera(context);
                    },
                    child: Text("Buka Kamera"),
                  ),
                  RaisedButton(
                    onPressed: (){
                      if (imageFile==null) {
                        Toast.show("Gambar Masih Kosong", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                      }else {
                        _saveImage(imageFile);
                      }
                    },
                    child: Text("Simpan"),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
