import 'package:zanul_pdam/model/user.dart';
import 'package:zanul_pdam/resources/api_provider.dart';

class Repository {
  final apiProvider = ApiProvider();

  Future<List<User>> fetchAllUser() => apiProvider.fetchDataUser();

}