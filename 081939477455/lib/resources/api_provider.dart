import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client;
import 'dart:async';
import 'package:zanul_pdam/model/user.dart';


class ApiProvider {

  Client client = Client();
  final url = 'http://jsonplaceholder.typicode.com/users';

  Future<List<User>> fetchDataUser() async{
    var response = await client.get(url);
    if (response.statusCode == 200) {
      return compute(userFromJson, response.body);
    }else {
      throw Exception("error");
    }
  }
}