
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:zanul_pdam/ui/apidisplay.dart';
import 'package:zanul_pdam/ui/imagedisplay.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 1 / 3,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("images/Scooter.jpg"),
                              fit: BoxFit.cover)),
                      child: Center(
                        child: ListTile(
                          leading: ClipOval(
                            child: Icon(Icons.perm_identity, size: 50,),
                          ),
                          title: Text(
                            "Zanul Harir",
                            style: TextStyle(fontFamily: "Poppins"),
                          ),
                          subtitle:
                          Text("0819393477455", style: TextStyle(fontFamily: "Poppins")),
                          onTap: () {},

                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 2 / 3,
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 10,),
                          Text(
                            "PDAM TES",
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 35),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 1 / 4),
                  decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(20)),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 1 / 3.5,
                  child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Main Menus",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  InkWell(
                                    splashColor: Colors.white,
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ApiDisplay()));
                                    },
                                    child: Container(
                                      width: (MediaQuery.of(context).size.height *
                                          1 /
                                          4) /
                                          3,
                                      height: (MediaQuery.of(context).size.height *
                                          1 /
                                          4) /
                                          3,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle),
                                      child: Icon(Icons.accessibility),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Data API",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),

                              Column(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ImageDisplay()));
                                    },
                                    splashColor: Colors.white,
                                    child: Container(
                                        width: (MediaQuery.of(context).size.height *
                                            1 /
                                            4) /
                                            3,
                                        height:
                                        (MediaQuery.of(context).size.height *
                                            1 /
                                            4) /
                                            3,
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle),
                                        child: Icon(Icons.camera_alt)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Camera",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
